package com.example.droidcafewsettings;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatImageView;
import android.view.Menu;
import android.widget.Button;

import static org.hamcrest.CoreMatchers.equalTo;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.robolectric.Shadows.shadowOf;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadow.api.Shadow;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;
import org.robolectric.shadows.ShadowToast;

import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

@RunWith(RobolectricTestRunner.class)

public class MainActivityTest {

    private MainActivity activity;
    private OrderActivity activityOrder;
    private ImageView orderFroyoImg;
    private FloatingActionButton fabButton;

    private EditText nameED;
    private EditText addressED;
    private EditText phoneED;
    private TextView tv;
    private Button saveCustomerInfoButton;


    @Before
    public void setUp() throws Exception {
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void TC1() throws Exception {
                 orderFroyoImg = (ImageView) activity.findViewById(R.id.froyo);
                orderFroyoImg.performClick();
        //Assert.assertEquals("123". ShadowToast.getTextOfLatestToast());
        assertThat(ShadowToast.getTextOfLatestToast().toString(), equalTo("You ordered a FroYo."));

    }

    @Test
    public  void TC2() throws Exception{

        fabButton = (FloatingActionButton) activity.findViewById(R.id.fab);
        fabButton.performClick();

        //assertThat(activity, new Inte());

        // opening next activity for writing customer info
        Intent startedIntent = shadowOf(activity).getNextStartedActivity();
        ShadowIntent shadowIntent = shadowOf(startedIntent);
        assertEquals(OrderActivity.class, shadowIntent.getIntentClass());

        activityOrder = Robolectric.buildActivity(OrderActivity.class).create().resume().get();

        // Pushing customer Information
        nameED = (EditText) activityOrder.findViewById(R.id.name_text);
        //tv = (TextView) activityOrder.findViewById(R.id.order_textview);

        nameED.setText("abc");

        //nameED.requestFocus();
        //nameED.text
        addressED = (EditText) activityOrder.findViewById(R.id.address_text);
        addressED.setText("101 packard blvd");
        phoneED = (EditText) activityOrder.findViewById(R.id.phone_text);
        phoneED.setText("1234567890");

        saveCustomerInfoButton = (Button) activityOrder.findViewById(R.id.saveButton);
        saveCustomerInfoButton.performClick();

        ShadowActivity activityShadowBack = Shadows.shadowOf(activityOrder);

        activityOrder.onBackPressed();
        assertTrue(activityShadowBack.isFinishing());

        fabButton.performClick();

        //assertThat(activity, new Inte());

        // opening next activity for writing customer info
        Intent startedIntent2 = shadowOf(activity).getNextStartedActivity();
        ShadowIntent shadowIntent2 = shadowOf(startedIntent2);
        assertEquals(OrderActivity.class, shadowIntent2.getIntentClass());

        assertEquals("abc", nameED.getText().toString());
        assertEquals("101 packard blvd", addressED.getText().toString());
        assertEquals("1234567890", phoneED.getText().toString());



    }




}
